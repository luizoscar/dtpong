#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
###############################################################################################'
 DTPong.py

MIT License

Copyright (c) 2019 Luiz Oscar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

###############################################################################################'
 @class Main class, used as the application entry point.
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''

###############
# PyGObject - Must be the first import
###############
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
from gi.repository import Gtk

###############

import getopt
import os
import sys
import logging
from modules.utils.AppSettings import AppSettings
from modules.dialogs.MainWindow import MainWindow
 
def main(argv):
    
    # Log file
    dir_app = os.path.dirname(os.path.realpath(__file__))
    arquivo_log = dir_app + os.sep + "application.log"
    
    fs = '%(asctime)s %(message)s'
    fmt = logging.Formatter(fs, None)
    # Output log to file
    logging.basicConfig(level=logging.INFO, format=fs, filename=arquivo_log, filemode='w')
    
    # Add a handler to capture stderr
    root_logger = logging.getLogger()
    hdlr = logging.StreamHandler(sys.stderr)
    hdlr.setFormatter(fmt)
    root_logger.addHandler(hdlr)
    
    # Enable debug mode
    debug_mode = False
    
    # Parse the application params
    try:
        opts = getopt.getopt(argv, "hd", [])
    except getopt.GetoptError:
        print('DTPong.py -h (help) -d (debug)')
        sys.exit(2)
    for opt in opts[0]:
        if opt[0] == '-h':
            print("DTPong scoreboard")
            print("Usage: DTPong.py -h (help) -d (debug)")
            sys.exit()
        elif opt[0] == '-d':
            debug_mode = True
    
    if sys.version_info < (3, 0):
        raise Exception("This application is intended to run on Python 3")

    # Load the application settings
    settings = AppSettings()
    settings.set_debug_mode(debug_mode)
    
    # Show de main window 
    main_window = MainWindow()
    main_window.show_all()
    Gtk.main()


if __name__ == '__main__':
    main(sys.argv[1:])
