# -*- coding: utf-8 -*-
'''
###############################################################################################'
 AppSettings.py

MIT License

Copyright (c) 2019 Luiz Oscar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


###############################################################################################'
 @class Module that saves and retrieve the application settings
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''

from gi.repository import Gtk, Gdk

import os
from modules.utils.XmlManager import XmlManager
from modules.utils.constants.Titles import Titles

class AppSettings(XmlManager):

    _css_context = None
    _label_dict = None
    _player_dict = None

    # Game modes
    GAME_MODE_TOURNAMENT = 0
    GAME_MODE_PLAY_NO_STOP = 1
    GAME_MODE_PLAY_3_WIN_1_OUT = 2
    GAME_MODE_PLAY_3_WIN_2_OUT = 3
    
    # Default keyboards keys
    _KEY_P1_UP = 49  # 1
    _KEY_P1_DOWN = 50  # 2
    _KEY_P2_UP = 51  # 3
    _KEY_P2_DOWN = 52  # 4 
    _KEY_SELECT = 32  # Space
    
    # Default GPIO ports
    _GPIO_P1_UP = 17
    _GPIO_P1_DOWN = 27
    _GPIO_SELECT = 4
    _GPIO_P2_UP = 26
    _GPIO_P2_DOWN = 19 
    
    # Number of points to win a set
    _SET_POINTS = 11
    
    # Current game mode
    _GAME_MODE = 1
    
    # Player keeps serving
    _winner_keeps_serving = False

    # XML Nodes
    _XML_NODE_CONFIG = 'config'
    _XML_NODE_LABELS = 'labels'
    _XML_NODE_DEBUG = 'debug'
    _XML_NODE_GAME_SET_POINTS = "set_points"
    _XML_MODE_GAME_MODE_1_TYPE = "game_mode_play_type"
    _XML_MODE_WINNER_KEEPS_SERVING = "winner_keeps_serving"

    # Mapped keyboard key code
    _XML_NODE_KEY_CODE_P1_UP = "key_code_p1_up"
    _XML_NODE_KEY_CODE_P2_UP = "key_code_p2_up"
    _XML_NODE_KEY_CODE_P1_DOWN = "key_code_p1_down"
    _XML_NODE_KEY_CODE_P2_DOWN = "key_code_p2_down"
    _XML_NODE_KEY_CODE_SELECT = "key_code_select"

    # Mapped keyboard key code
    _XML_NODE_GPIO_PIN_P1_UP = "gpio_pin_p1_up"
    _XML_NODE_GPIO_PIN_P2_UP = "gpio_pin_p2_up"
    _XML_NODE_GPIO_PIN_P1_DOWN = "gpio_pin_p1_down"
    _XML_NODE_GPIO_PIN_P2_DOWN = "gpio_pin_p2_down"
    _XML_NODE_GPIO_PIN_SELECT = "gpio_pin_select"

    _XML_NODE_PLAYER = 'player'
    _XML_NODE_PLAYERS = 'players'
    _XML_ATTR_RFID = 'rfid'
    _XML_ATTR_NAME = 'name'
    _XML_ATTR_EMAIL = 'email'
            
    # Application dir
    _APP_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    _CSS_FILE = _APP_DIR + os.sep + "settings.css"
    _XML_FILE = _APP_DIR + os.sep + "settings.xml"
    _PLAYER_LIST = _APP_DIR + os.sep + "players.xml"
    
    def __init__(self):
        '''
        Default constructor
        '''
        super(AppSettings, self).__init__(self._XML_FILE)
        
        if not os.path.isfile(self._CSS_FILE):
            raise Exception("Failed to locate the application CSS file: " + str(self._CSS_FILE))
        
        # Create the settings with the default values
        if not os.path.isfile(self._XML_FILE):
            self._create_settings_xml()

        if not os.path.isfile(self._PLAYER_LIST):
            self._create_players_xml()

        ####################
        # Load the application settings
        ####################

        # Load the keyboard codes
        self._KEY_P1_UP = int(self.get_app_settings(self._XML_NODE_KEY_CODE_P1_UP))
        self._KEY_P2_UP = int(self.get_app_settings(self._XML_NODE_KEY_CODE_P2_UP))
        self._KEY_P1_DOWN = int(self.get_app_settings(self._XML_NODE_KEY_CODE_P1_DOWN))
        self._KEY_P2_DOWN = int(self.get_app_settings(self._XML_NODE_KEY_CODE_P2_DOWN))
        self._KEY_SELECT = int(self.get_app_settings(self._XML_NODE_KEY_CODE_SELECT))

        # Load the GPIO codes
        self._GPIO_P1_UP = int(self.get_app_settings(self._XML_NODE_GPIO_PIN_P1_UP))
        self._GPIO_P2_UP = int(self.get_app_settings(self._XML_NODE_GPIO_PIN_P2_UP))
        self._GPIO_P1_DOWN = int(self.get_app_settings(self._XML_NODE_GPIO_PIN_P1_DOWN))
        self._GPIO_P2_DOWN = int(self.get_app_settings(self._XML_NODE_GPIO_PIN_P2_DOWN))
        self._GPIO_SELECT = int(self.get_app_settings(self._XML_NODE_GPIO_PIN_SELECT))

        # Load the game settings
        self._SET_POINTS = int(self.get_app_settings(self._XML_NODE_GAME_SET_POINTS))
        self._GAME_MODE_1_TYPE = int(self.get_app_settings(self._XML_MODE_GAME_MODE_1_TYPE))
        self._winner_keeps_serving = 'true' ==  self.get_app_settings(self._XML_MODE_WINNER_KEEPS_SERVING).lower()
    
    def _create_players_xml(self):
        """
        Create a sample players.xml
        """
        manager = XmlManager(self._PLAYER_LIST)
        root_node = manager.create_element(self._XML_NODE_PLAYERS)

        p1_node = manager.create_element(self._XML_NODE_PLAYER, root_node)
        manager.create_attribute(p1_node, self._XML_ATTR_RFID, "1234567890")
        manager.create_attribute(p1_node, self._XML_ATTR_NAME, "Player 1")
        manager.create_attribute(p1_node, self._XML_ATTR_EMAIL, "sample@email.com")
        
        p2_node = manager.create_element(self._XML_NODE_PLAYER, root_node)
        manager.create_attribute(p2_node, self._XML_ATTR_RFID, "0000000000")
        manager.create_attribute(p2_node, self._XML_ATTR_NAME, "Player 2")
        manager.create_attribute(p2_node, self._XML_ATTR_EMAIL, "other@email.com")
        
        manager.save(root_node)        
        
    
    def _create_settings_xml(self):
        """
        Create the Settings.xml with the default values
        """
            
        ####################
        # Application settings
        ####################

        self.set_app_settings(self._XML_NODE_GAME_SET_POINTS, "11")
        self.set_app_settings(self._XML_MODE_GAME_MODE_1_TYPE, "2")
        self.set_app_settings(self._XML_MODE_WINNER_KEEPS_SERVING,"False")
        
        # Save the keyboard mapping
        self.set_app_settings(self._XML_NODE_KEY_CODE_P1_UP, str(self._KEY_P1_UP))
        self.set_app_settings(self._XML_NODE_KEY_CODE_P2_UP, str(self._KEY_P2_UP))
        self.set_app_settings(self._XML_NODE_KEY_CODE_P1_DOWN, str(self._KEY_P1_DOWN))
        self.set_app_settings(self._XML_NODE_KEY_CODE_P2_DOWN, str(self._KEY_P2_DOWN))
        self.set_app_settings(self._XML_NODE_KEY_CODE_SELECT, str(self._KEY_SELECT))

        # Save the GPIO pin mapping
        self.set_app_settings(self._XML_NODE_GPIO_PIN_P1_UP, str(self._GPIO_P1_UP))
        self.set_app_settings(self._XML_NODE_GPIO_PIN_P2_UP, str(self._GPIO_P2_UP))
        self.set_app_settings(self._XML_NODE_GPIO_PIN_P1_DOWN, str(self._GPIO_P1_DOWN))
        self.set_app_settings(self._XML_NODE_GPIO_PIN_P2_DOWN, str(self._GPIO_P2_DOWN))
        self.set_app_settings(self._XML_NODE_GPIO_PIN_SELECT, str(self._GPIO_SELECT))
        
        ####################
        # Application labels
        ####################
        
        # Console Buttons
        self.set_label(Titles.BTN_P1_PLUS, "P1 +")
        self.set_label(Titles.BTN_P2_PLUS, "P2 +")
        self.set_label(Titles.BTN_P1_MINUS, "P1 -")
        self.set_label(Titles.BTN_P2_MINUS, "P2 -")            
        self.set_label(Titles.BTN_SELECT, "Selecionar")            
        
        # Main Window
        self.set_label(Titles.APPLICATION_TITLE, "DTPong 1.0")
        self.set_label(Titles.DEFAULT_PLAYER_1_NAME, "Jogador 1")
        self.set_label(Titles.DEFAULT_PLAYER_2_NAME, "Jogador 2")
        self.set_label(Titles.NEXT_PLAYERS, "Próximos Jogadores da Pelada")
        
        self.set_label(Titles.GAME_MODE_TOURNAMENT, "Desafio")
        self.set_label(Titles.GAME_MODE_PLAY_NO_STOP, "Pelada")
        self.set_label(Titles.GAME_MODE_PLAY_3_WIN_1_OUT, "Pelada 3V / 1F")
        self.set_label(Titles.GAME_MODE_PLAY_3_WIN_2_OUT, "Pelada 3V / 2F")

        # Main Menu
        self.set_label(Titles.START_NEW_GAME, "Criar uma nova partida")
        self.set_label(Titles.APPEND_NEW_PLAYER, "Adicionar jogador à fila")
        self.set_label(Titles.REMOVE_EXISTING_USER, "Remover jogador da fila")
        self.set_label(Titles.REGISTER_RFID_USER, "Registrar o RFID do jogador")
        
        # New Game Dialog
        self.set_label(Titles.NEW_GAME_MODE_TOURNAMENT, "Novo Desafio")
        self.set_label(Titles.NEW_GAME_MODE_PLAY_NO_STOP, "Nova Pelada sem esperas")
        self.set_label(Titles.NEW_GAME_MODE_PLAY_3_WIN_1_OUT, "Nova Pelada esperando 1 após 3 vitórias")
        self.set_label(Titles.NEW_GAME_MODE_PLAY_3_WIN_2_OUT, "Nova Pelada esperando 2 após 3 vitórias")

        # Multiple Selection Dialog 
        self.set_label(Titles.MULTIPLE_SELECTION_DIALOG_TITLE, "Configurações do sistema")
        self.set_label(Titles.MULTIPLE_SELECTION_DIALOG_DESCRIPTION, "Selecione a opção desejada com os botões correspondentes")
        
        # Game over Dialog
        self.set_label(Titles.END_GAME_DIALOG_TITLE, "Fim de jogo")
        self.set_label(Titles.GAME_OVER_DIALOG_WINNER_MSG, "Fim de jogo!")
        self.set_label(Titles.END_GAME_DIALOG_WINNER, "O vencedor do jogo foi:");
        
        # User Reader Dialog
        self.set_label(Titles.PLAYER_READER_DIALOG, "Consulta de jogador")
        self.set_label(Titles.PLAYER_READER_DIALOG_DESCRIPTION, "Aproxime o crachá do leitor de RFID")
        self.set_label(Titles.PLAYER_READER_DIALOG_INSTRUCTIONS, "<<< >>>")
        self.set_label(Titles.PLAYER_READER_DIALOG_NOT_FOUND, "Usuário não cadastrado:")
        self.set_label(Titles.PLAYER_READER_DIALOG_RFID_INSTRUCTIONS,"Aproxime o crachá")

        # User selection Dialog
        self.set_label(Titles.PLAYER_SELECTION_DIALOG_TITLE, "Seleção dos desafiantes")
        self.set_label(Titles.PLAYER_SELECTION_DIALOG_DESCRIPTION, "Pressione o botão ao lado e aproxime o crachá do leitor")
        self.set_label(Titles.PLAYER_SELECTION_DIALOG_INSTRUCTIONS, "<<< >>>")
        self.set_label(Titles.PLAYER_SELECTION_DIALOG_NOT_FOUND, "Usuário não cadastrado:")

        # Player remove Dialog
        self.set_label(Titles.PLAYER_REMOVE_DIALOG_TITLE, "Remoção de Jogador")
        self.set_label(Titles.PLAYER_REMOVE_DIALOG_DESCRIPTION, "Selecione o usuário ou navegue para a próxima página")
        self.set_label(Titles.PLAYER_REMOVE_DIALOG_NEXT_PAGE, "Exibir a próxima página")
        
    def get_player_list(self):
        """
        Get the list of players from the XML file
        """
        if self._player_dict is None:
            self._player_dict = {}
            for element in XmlManager(self._PLAYER_LIST).get_root_node():
                self._player_dict[element.get(self._XML_ATTR_NAME)] = element.get(self._XML_ATTR_RFID) 
        
        return self._player_dict
    
    def register_player(self, info):
        """
        Register the player name and RFID
        """
        name = info[0]
        rfid = info[1]
        email = ''
        
        manager = XmlManager(self._PLAYER_LIST)
        root_node = manager.get_root_node()
        for element in root_node:
            if element.get(self._XML_ATTR_NAME) == name:
                email = element.get(self._XML_ATTR_EMAIL)
                root_node.remove(element)
        new_node = manager.create_element(self._XML_NODE_PLAYER, root_node)
        manager.create_attribute(new_node, self._XML_ATTR_RFID, rfid)
        manager.create_attribute(new_node, self._XML_ATTR_NAME, name)
        manager.create_attribute(new_node, self._XML_ATTR_EMAIL, email)
        manager.save(root_node)
         
    def is_winner_keeps_serving(self):
        """
        Check if the winner will keep serving
        """
        return self._winner_keeps_serving

    def set_game_mode(self, new_game_mode):
        """
        Set the new game mode
        """
        self._GAME_MODE = new_game_mode
    
    def get_game_mode(self):
        """
        Get the game mode
        """
        return self._GAME_MODE
    
    def get_set_points(self):
        """
        Get the number of points to end a set
        """
        return self._SET_POINTS
    
    def load_css_context(self):
        """
        Load the CSS context
        """

        if self._css_context is None:
            #Load the CSS file
            with open(self._CSS_FILE, 'r') as myfile:
                data = myfile.read()
            css_provider = Gtk.CssProvider()
            css_provider.load_from_data(bytes(data, 'utf-8'))
            
            # Set the CSS context
            self._css_context = Gtk.StyleContext()
            self._css_context.add_provider_for_screen(Gdk.Screen.get_default(), css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)        
    

    def set_label(self, xml_node_name, label_value):
        """
        Save an application label
        
        @param xml_node_name: The label node name
        @param label_value: The label value
        """
        if label_value is None or xml_node_name is None:
            return

        root_node = self.get_root_node() if os.path.isfile(self.xml_file) else self.create_element(self._XML_NODE_CONFIG)
        label_node = self.get_element(root_node, self._XML_NODE_LABELS)
        if label_node is None:
            label_node = self.create_element(self._XML_NODE_LABELS, root_node)
        
        # If the value is not null, add a new node
        if str(label_value).strip():
            self.create_unique_element(xml_node_name, label_node).text = label_value
    
        self.save(root_node)
    
    def get_label(self, xml_node_name=None):
        """
        Get a label text
        
        @param xml_node_name: The XML node name
        """
        if xml_node_name is None:
            return None
        
        # Load the labels to the memory
        if self._label_dict is None:
            self._label_dict = {}
            for node in self.get_element_list("./" + self._XML_NODE_LABELS + "/"):
                self._label_dict[node.tag] = node.text.strip()
        
        return self._label_dict[xml_node_name] if xml_node_name in self._label_dict else None  
        
    def set_app_settings(self, name, value):
        """
        Set an application settings
        
        @param name: The settings name
        @param value: The settings value
        """
        root_node = self.get_root_node() if os.path.isfile(self.xml_file) else self.create_element(self._XML_NODE_CONFIG)
    
        # If the value is not null, add a new node
        if value is not None and str(value).strip():
            self.create_unique_element(name, root_node).text = value
        else:
            root_node = self.remove_element(name)
    
        self.save(root_node)
    
    def get_app_settings(self, xml_tag=None):
        """
        Get an application settings
        
        @param xml_tag: The XML attribute name
        """
        if xml_tag is None:
            return None
                
        node_caminho = self.get_element_list("./" + xml_tag)
        return None if not node_caminho or node_caminho[0].text is None else node_caminho[0].text.strip()

    def set_debug_mode(self, debug_mode):
        '''
        Set the application debug mode
        
        @param debug_mode: True to debug mode
        '''
        self.set_app_settings(AppSettings._XML_NODE_DEBUG, str(debug_mode))
    
    def is_debug_mode(self):
        '''
        Check if the application is in debug mode
        '''
        return 'True' == self.get_app_settings(AppSettings._XML_NODE_DEBUG)
