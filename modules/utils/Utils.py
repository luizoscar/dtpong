# -*- coding: utf-8 -*-

'''
###############################################################################################'
 Utils.py

MIT License

Copyright (c) 2019 Luiz Oscar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


###############################################################################################'
 @class Utility class
 @autor: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
import re
import sys
import datetime
import time

class Utils(object):

    def to_unicode(self, text):
        '''
        @param text: The text to be converted
        Convert a text to Unicode
        '''
        if text is None:
            return None
        
        if sys.version_info < (3, 0):
            try:
                return None if text is None else str(text).decode("utf-8")
            except UnicodeDecodeError:
                # Para suporte a caracteres especiais no Windows
                return None if text is None else str(text).decode('latin-1').encode("utf-8")

        return str(text)
        
    def from_unicode(self, text):
        '''
        @param text: The text to be converted
        Convert a text From Unicode
        '''
        if sys.version_info < (3, 0):
            return None if text is None else text.decode("utf-8")
        else:
            return None if text is None else str(text)

    def natural_sort(self, l): 
        convert = lambda text: int(text) if text.isdigit() else text.lower() 
        alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
        return sorted(l, key=alphanum_key)        
    
    def get_current_time(self):
        return datetime.datetime.now().strftime('%d/%m/%Y %H:%M:%S')
    
    def get_formated_minutes(self, time_in_seconds):
        return time.strftime('%H:%M:%S', time.gmtime(time_in_seconds))
