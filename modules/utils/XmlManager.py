# -*- coding: utf-8 -*-
'''
###############################################################################################'
 XmlManager.py

MIT License

Copyright (c) 2019 Luiz Oscar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


###############################################################################################'
 @class Base Module to save and retrieve settings from XML files
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''

from lxml import etree as ET
import os

class XmlManager(object):
    '''
    Module used to sort, edit and retrieve XML data.
    '''

    def __init__(self, xml_file):
        '''
        Default constructor
        
        @param xml_file: The path to the XML file
        '''
        self.xml_file = xml_file
    
    def create_attribute(self, element, attribute_name, attribute_value):
        '''
        Create a new Attribute on an existing Element.
        
        @param element: The existing XML Element.
        @param attribute_name: The attribute name.
        @param attribute_value: The attribute value.
        '''
        element.set(attribute_name, str(attribute_value))

    def remove_element(self, name):
        '''
        Remove an existing element
        
        @param name: The Element name.
        '''

        if os.path.isfile(self.xml_file):
            root_node = self.get_root_node()
        
            # Remove the existing node
            node = root_node.find("./" + name)
            if node is not None:
                root_node.remove(node)
            
            return root_node
        
        return None
    
    def get_element(self, parent, path):
        '''
        Retrieve a XML Element using XPATH

        @param path: The element XPATH.
        @param parent: The parent Element
        '''
        
        return parent.find(path)
                
    def create_unique_element(self, name, parent=None):
        '''
        Create a new Element, without duplications
        
        @param name: The new element Name.    
        @param parent: The parent Element, None to create a root Element
        '''
                   
        if parent is None:
            return ET.Element(name)
        else:
            anterior = parent.find(name) 
            if anterior is not None:
                parent.remove(anterior)
    
            return ET.SubElement(parent, name)

    def create_element(self, name, parent=None):
        '''
        Create a new Element, allowing duplications
        
        @param name: The new element Name.    
        @param parent: The parent Element, None to create a root Element
        '''
        if parent is None:
            return ET.Element(name)
        else:
            return ET.SubElement(parent, name)
    
    def save(self, root_node):
        """
        Format and save a XML file
        
        @param root_node: The root Element.
        """
        self.indent_xml(root_node)
        pretty_xml = ET.tostring(root_node, encoding="UTF-8", method="xml", xml_declaration=True)
        arquivo = open(self.xml_file, "wb")
        arquivo.write(pretty_xml)
        arquivo.close()
    
    def get_root_node(self):
        '''
        Retrieve the root Element.
        '''
        tree = ET.parse(self.xml_file, ET.XMLParser(remove_comments=False, strip_cdata=False))
        return tree.getroot()
    
    def get_att_value(self, element, att_name):
        '''
        Get the value from an attribute

        @param element: The XML element
        @param att_name: The attribute name
        '''
        return element.get(att_name)

    def get_element_text(self, element):
        '''
        Get the text from a Element

        @param element: The XML element
        '''
        
        return self.trim_cdata(element.text)
        
    def get_attribute_values(self, path, att_name):
        '''
        Get a list with the values from the attributes on the list of elements on the path   

        @param path: The XPATH
        @param att_name: The name of the attribute
        '''
        
        resp = []
        for item in self.get_element_list(path): 
            resp.append(self.get_att_value(item, att_name))
            
        return resp
    
    def get_element_list(self, path):
        '''
        Get a list of Elements from a given path

        @param path: The path to the elements
        '''
        resp = []
        for item in ET.parse(self.xml_file, ET.XMLParser(remove_comments=False, strip_cdata=False)).findall(path):
            resp.append(item)
            
        return resp           

    def indent_xml(self, elem, level=0):
        """
        Indent a XML file using the built-in LXML module

        @param elem: The XML element
        @param level: The number of tabs
        """
        i = "\n" + level * "\t"
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "\t"
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for elem in elem:
                self.indent_xml(elem, level + 1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = i   
    
    def trim_cdata(self, cdata):
        """
        Remove the CDATA trailing spaces
        
        @param cdata: The CDATA content
        """
        resp = cdata
        if resp is not None:
            resp = ' '.join([line.strip() for line in resp.strip().splitlines()])
    
        return resp
  
    def set_cdata_value(self, elem, value):
        '''
        Add a CDATA text to a XML node
        
        @param elem: The XML element
        @param value: The CDATA value
        '''
        elem.text = ET.CDATA(value)        
