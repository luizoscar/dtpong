# -*- coding: utf-8 -*-

'''
###############################################################################################'
 CSSClass.py

MIT License

Copyright (c) 2019 Luiz Oscar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


###############################################################################################'
 @class Class with the CSS class name
 @autor: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''

class CSSClass:

    # Window and Dialogs
    MAIN_WINDOW = "main_window"
    DIALOGS = "dialogs"
    
    # Console Buttons
    BTN_BASE = "btn_base"
    BTN_SELECT = "btn_select"
    BTN_P1_PLUS = "btn_p1_plus"
    BTN_P2_PLUS = "btn_p2_plus"
    BTN_P1_MINUS = "btn_p1_minus"
    BTN_P2_MINUS = "btn_p2_minus"

    # Main Window
    PLAYER_1_SET_COUNTER = "player_1_set_counter"
    PLAYER_2_SET_COUNTER = "player_2_set_counter"
    PLAYER_SERVE_INDICATOR = "player_serve_indicator"
    PLAYER_1_NAME = "player_1_name"
    PLAYER_2_NAME = "player_2_name"
    PLAYER_1_SCORE = "player_1_score"
    PLAYER_2_SCORE = "player_2_score"
    GAME_MODE = "game_mode"
    NEXT_PLAYERS="next_players"
    CURRENT_TIME = "current_time"
    GAME_DURATION = "game_duration"
    NEXT_PLAYER_NAME="next_player_name"

    # Multiple Selection Dialog 
    UNSELECTED_ITEM = "unselected_item"
    SELECTED_ITEM= "selected_item"
    MULTIPLE_SELECTION_DIALOG_DESCRIPTION = "multiple_selection_dialog_description"

    # User selection Dialog
    USER_SELECTION_DIALOG_SELECTED_USER = "player_selection_dialog_selected_user"
    PLAYER_SELECTION_DIALOG_DESCRIPTION="player_selection_dialog_description"

    # User Reader Dialog
    PLAYER_READER_DIALOG_DESCRIPTION="player_reader_dialog_description"
    PLAYER_READER_RFID = "player_reader_rfid"
    PLAYER_READER_NAME = "player_reader_name"

    # Game over Dialog
    GAME_OVER_DIALOG_DESCRIPTION="game_over_dialog_description"
    GAME_OVER_DIALOG_WINNER_MSG="game_over_dialog_winner_msg"
    GAME_OVER_DIALOG_WINNER_NAME ="game_over_dialog_winner_name"
    GAME_OVER_DIALOG_WINNER_SETS = "game_over_dialog_winner_sets"
    
    #Player remove Dialog
    PLAYER_REMOVE_DIALOG_DESCRIPTION = "player_remove_dialog_description"

