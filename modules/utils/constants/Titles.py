# -*- coding: utf-8 -*-

'''
###############################################################################################'
 Titles.py

MIT License

Copyright (c) 2019 Luiz Oscar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


###############################################################################################'
 @class Class with the application titles
 @autor: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''


class Titles:
        
    # Console Buttons
    BTN_SELECT = "btn_select"
    BTN_P1_PLUS = "btn_p1_plus"
    BTN_P2_PLUS = "btn_p2_plus"
    BTN_P1_MINUS = "btn_p1_minus"
    BTN_P2_MINUS = "btn_p2_minus"

    # Main Window
    APPLICATION_TITLE = "application_title"
    PLAYER_1_SERVE = "player_1_serve"
    PLAYER_2_SERVE = "player_2_serve"
    DEFAULT_PLAYER_1_NAME = "default_player_1_name"
    DEFAULT_PLAYER_2_NAME = "default_player_2_name"
    NEXT_PLAYERS = "next_players"
    
    GAME_MODE_TOURNAMENT = "game_mode_tournament"
    GAME_MODE_PLAY_NO_STOP = "game_mode_play_no_stop"
    GAME_MODE_PLAY_3_WIN_1_OUT = "game_mode_play_3_win_1_out"
    GAME_MODE_PLAY_3_WIN_2_OUT = "game_mode_play_3_win_2_out"
    
    # Main Menu
    START_NEW_GAME = "start_new_game"
    APPEND_NEW_PLAYER = "append_new_player"
    REMOVE_EXISTING_USER = "remove_existing_user"
    REGISTER_RFID_USER = "register_rfid_user"

    # New game Dialog
    NEW_GAME_MODE_TOURNAMENT = "new_game_mode_tournament"
    NEW_GAME_MODE_PLAY_NO_STOP = "new_game_mode_play_no_stop"
    NEW_GAME_MODE_PLAY_3_WIN_1_OUT = "new_game_mode_play_3_win_1_out"
    NEW_GAME_MODE_PLAY_3_WIN_2_OUT = "new_game_mode_play_3_win_2_out"

    # Multiple Selection Dialog 
    MULTIPLE_SELECTION_DIALOG_DESCRIPTION = "multiple_selection_dialog_description"
    MULTIPLE_SELECTION_DIALOG_TITLE = "multiple_selection_dialog_title"

    # Player Reader Dialog
    PLAYER_READER_DIALOG = "user_reader_dialog"
    PLAYER_READER_DIALOG_NOT_FOUND = "user_reader_dialog_not_found"
    PLAYER_READER_DIALOG_DESCRIPTION = "user_reader_dialog_description"
    PLAYER_READER_DIALOG_INSTRUCTIONS = "user_reader_dialog_instructions" 
    PLAYER_READER_DIALOG_RFID_INSTRUCTIONS = "user_reader_dialog_rfid_instructions"
        
    # Game over Dialog
    GAME_OVER_DIALOG_WINNER_MSG = "end_game_dialog_description"
    END_GAME_DIALOG_WINNER = "end_game_dialog_winner"
    END_GAME_DIALOG_TITLE = "end_game_dialog_title"
    
    # Player selection Dialog
    PLAYER_SELECTION_DIALOG_TITLE = "user_selection_dialog_title"
    PLAYER_SELECTION_DIALOG_DESCRIPTION = "user_selection_dialog_description"
    PLAYER_SELECTION_DIALOG_INSTRUCTIONS = "user_selection_dialog_instructions"
    PLAYER_SELECTION_DIALOG_NOT_FOUND = "user_selection_dialog_not_found"

    # Player remove Dialog
    PLAYER_REMOVE_DIALOG_TITLE = "player_remove_dialog_title"
    PLAYER_REMOVE_DIALOG_DESCRIPTION = "player_remove_dialog_description"
    PLAYER_REMOVE_DIALOG_NEXT_PAGE = "player_remove_nex_page"
    
