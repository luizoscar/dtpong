# -*- coding: utf-8 -*-
'''
###############################################################################################'
 PlayerSelectionDialog.py

MIT License

Copyright (c) 2019 Luiz Oscar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


###############################################################################################'
 @class Dialog used to select two players 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from gi.repository import Gtk, GLib
from modules.dialogs.base.BaseContainer import BaseDialog
from modules.utils.constants.CSSClass import CSSClass
from modules.utils.constants.Titles import Titles
from modules.dialogs.players.PlayerReaderDialog import PlayerReaderDialog

class PlayerSelectionDialog(BaseDialog):
        
    _player_1 = None
    _player_2 = None
    _selected_item = 0
    
    def __init__(self, parent):

        super(PlayerSelectionDialog, self).__init__(title=self._app_settings.get_label(Titles.PLAYER_SELECTION_DIALOG_TITLE), transient_for=parent)
        
        self._load_container_css(CSSClass.DIALOGS)

        self.connect("key-press-event", self._on_button_pressed)
        
        # Main container
        self._main_grid = Gtk.Grid()
        self._main_grid.set_column_homogeneous(True)
        self._main_grid.set_row_homogeneous(True)

        # Add the labels
        row = 0;
        # New game text
        self._main_grid.attach(self._create_label(CSSClass.PLAYER_SELECTION_DIALOG_DESCRIPTION, self._app_settings.get_label(Titles.PLAYER_SELECTION_DIALOG_DESCRIPTION)), 0, row, 6, 1)
        
        row = 1;
        # P1 Button UP
        self._main_grid.attach(self._create_label(CSSClass.BTN_P1_PLUS, self._app_settings.get_label(Titles.BTN_P1_PLUS)), 0, row, 1, 1)

        # Player 1
        self._label_player_1 = self._create_label(CSSClass.UNSELECTED_ITEM, self._app_settings.get_label(Titles.PLAYER_SELECTION_DIALOG_INSTRUCTIONS))
        self._main_grid.attach(self._label_player_1, 1, row, 5, 1)

        row = 2;

        # P2 Button UP
        self._main_grid.attach(self._create_label(CSSClass.BTN_P2_PLUS, self._app_settings.get_label(Titles.BTN_P2_PLUS)), 0, row, 1, 1)

        # Player 2
        self._label_player_2 = self._create_label(CSSClass.UNSELECTED_ITEM, self._app_settings.get_label(Titles.PLAYER_SELECTION_DIALOG_INSTRUCTIONS))
        self._main_grid.attach(self._label_player_2, 1, row, 5, 1)

        row = 3;

        # Select button
        self._main_grid.attach(self._create_label(CSSClass.BTN_SELECT, self._app_settings.get_label(Titles.BTN_SELECT)), 1, row, 5, 1)

        self.get_content_area().pack_start(self._main_grid, True, True, 0)
        self._draw_screen()
        self.show_all()
    
    def _draw_screen(self):
        """
        Draw the labels on the screen
        """
        self._set_label_css(self._label_player_1, CSSClass.UNSELECTED_ITEM, self._app_settings.get_label(Titles.PLAYER_SELECTION_DIALOG_INSTRUCTIONS))
        self._set_label_css(self._label_player_2, CSSClass.UNSELECTED_ITEM, self._app_settings.get_label(Titles.PLAYER_SELECTION_DIALOG_INSTRUCTIONS))
        
        if self._player_1 is not None:
            self._set_label_css(self._label_player_1, CSSClass.USER_SELECTION_DIALOG_SELECTED_USER, self._player_1)

        if self._player_2 is not None:
            self._set_label_css(self._label_player_2, CSSClass.USER_SELECTION_DIALOG_SELECTED_USER, self._player_2)

    def _on_button_pressed(self, event, user_data):  # @UnusedVariable
        """
        Handle the keyboard key press
        @param event: The keyboard event
        @param user_data: The pressed user data
        """
        if user_data.keyval in self.get_button_codes():
            self.handle_button_action(user_data.keyval)
            
        return True
    
    def handle_button_action(self, code):
        """
        Handle the event caused by pressing a valid button
        """
        if self.is_select(code) and self._player_1 is not None and self._player_2 is not None:
            self.hide()
        else:
            if self.is_p1_up(code) or self.is_p1_down(code):
                GLib.idle_add(self._locate_p1)
            elif self.is_p2_up(code) or self.is_p2_down(code) :
                GLib.idle_add(self._locate_p2)

        return True
    
    def _locate_p1(self):
        self._player_1 = self._locate_player_name()
        GLib.idle_add(self._draw_screen)

    def _locate_p2(self):
        self._player_2 = self._locate_player_name()
        GLib.idle_add(self._draw_screen)

    def _locate_player_name(self):
        dialog = PlayerReaderDialog(self)
        info = dialog.do_show_and_get_info()
        self.start_monitor_gpio_buttons(self.handle_button_action)
        return info[0]
    
    def do_show_and_get_info(self):
        """
        Show the dialog and return the selected option
        """
        self.start_monitor_gpio_buttons(self.handle_button_action)
        
        while self._player_1 is None or self._player_2 is None:
            GLib.idle_add(self._draw_screen)
            self.run()
             
        self.do_close_screen()
        self.debug("Selected users: "+self._player_1+" vs "+self._player_2)
        return (self._player_1, self._player_2)
