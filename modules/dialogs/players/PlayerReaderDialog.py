# -*- coding: utf-8 -*-
'''
###############################################################################################'
 PlayerReaderDialog.py

MIT License

Copyright (c) 2019 Luiz Oscar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


###############################################################################################'
 @class Dialog used to select a single player 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from gi.repository import Gtk, GLib
from modules.dialogs.base.BaseContainer import BaseDialog
from datetime import datetime
from modules.utils.constants.CSSClass import CSSClass
from modules.utils.constants.Titles import Titles
from mfrc522.MFRC522 import MFRC522

class PlayerReaderDialog(BaseDialog):
        
    _player_name = None
    _player_rfid = None
    _reader = None
    _editing_player_name = None
    _unknow_player = None
    _players_dict = None
    
    def __init__(self, parent, editing_player_name=None):

        super(PlayerReaderDialog, self).__init__(title=self._app_settings.get_label(Titles.PLAYER_READER_DIALOG), transient_for=parent)
        self._editing_player_name = editing_player_name
        
        self._load_container_css(CSSClass.DIALOGS)

        self.connect("key-press-event", self._on_button_pressed)
        
        # Main container
        self._main_grid = Gtk.Grid()
        self._main_grid.set_column_homogeneous(True)
        self._main_grid.set_row_homogeneous(True)

        row = 0;

        # Description text
        self._main_grid.attach(self._create_label(CSSClass.PLAYER_READER_DIALOG_DESCRIPTION, self._app_settings.get_label(Titles.PLAYER_READER_DIALOG_DESCRIPTION)), 0, row, 6, 1)
        
        row = 1;

        # Player Name
        self._label_player_name = self._create_label(CSSClass.PLAYER_READER_NAME)
        self._main_grid.attach(self._label_player_name, 0, row, 6, 1)

        row = 2;

        # Player RFID
        self._label_player_rfid = self._create_label(CSSClass.PLAYER_READER_RFID)
        self._main_grid.attach(self._label_player_rfid, 0, row, 6, 1)

        row = 3;

        # Select button
        self._main_grid.attach(self._create_label(CSSClass.BTN_SELECT, self._app_settings.get_label(Titles.BTN_SELECT)), 1, row, 4, 1)

        self.get_content_area().pack_start(self._main_grid, True, True, 0)
    
        self._unknow_player = self._app_settings.get_label(Titles.PLAYER_READER_DIALOG_NOT_FOUND)
        self._player_name = self._create_random_name()
        
        try:
            self._reader = MFRC522()
        except FileNotFoundError:
            self._reader = None
            self.debug("RFID reader module not found!")

        self._players_dict = self._app_settings.get_player_list()
    
        # Start the RFID reader thread
        GLib.timeout_add_seconds(0.5,self._rfid_reader)

        self._draw_screen()
        self.show_all()
    
    def _rfid_reader(self):
        if self._reader is None:
            return False
        
        # Check if any card was detected
        status, tag_type = self._reader.MFRC522_Request(self._reader.PICC_REQIDL)
    
        if status == self._reader.MI_OK:
            # Read the card UID 
            status, uid = self._reader.MFRC522_Anticoll()
 
            if status == self._reader.MI_OK:
                uid = ''.join(['%X' % x for x in uid])
                self.debug(" Card detected: "+uid)
                
                self._player_rfid = uid
                self._player_name = self._unknow_player
                for item in self._players_dict.items():
                    if item[1] == uid:
                        self._player_name = item[0]
                GLib.idle_add(self._draw_screen)

        return self.is_visible()
    
    def _draw_screen(self):
        """
        Draw the labels on the screen
        """
        self._player_name = self._player_name if self._editing_player_name is None else self._editing_player_name         
        self._label_player_name.set_text(self._player_name)    
        self._label_player_rfid.set_text(self._app_settings.get_label(Titles.PLAYER_READER_DIALOG_RFID_INSTRUCTIONS) if self._player_rfid is None else self._player_rfid)    
    
    def _create_random_name(self):
        if self._player_name is None or self._player_name == self._unknow_player:
            return "Player "+datetime.now().strftime('%M%S')
        else: 
            return self._player_name
            
    
    def _on_button_pressed(self, event, user_data):  # @UnusedVariable
        """
        Handle the keyboard key press
        @param event: The keyboard event
        @param user_data: The pressed user data
        """
        if user_data.keyval in self.get_button_codes():
            self.handle_button_action(user_data.keyval)
            
        return True

    def handle_button_action(self, code):
        """
        Handle the event caused by pressing a valid button
        """
        if self.is_select(code) and self._player_name is not self._unknow_player:
            self.hide()
        elif self._player_name is self._unknow_player:
            self._player_name = self._create_random_name()
            self._player_rfid = None
            GLib.idle_add(self._draw_screen)
    
    def do_show_and_get_info(self):
        """
        Show the dialog and return the selected option
        """
        self.start_monitor_gpio_buttons(self.handle_button_action)        
        self.run()
        while self._player_name is None :
            GLib.idle_add(self._draw_screen)
            self.run()
             
        self.do_close_screen()
        
        self._player_rfid = "000000000000" if self._player_rfid is None else self._player_rfid
        self._player_name = self._player_name if self._player_name.find("/") < 0 else self._player_name[:self._player_name.find("/")].strip()  
        
        info = (self._player_name, self._player_rfid)
        self.debug("User selected: "+str(info))
        return info
