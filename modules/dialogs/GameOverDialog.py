# -*- coding: utf-8 -*-
'''
###############################################################################################'
 GameOverDialog.py

MIT License

Copyright (c) 2019 Luiz Oscar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


###############################################################################################'
 @class Dialog used to show the end game message 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from gi.repository import Gtk
from modules.dialogs.base.BaseContainer import BaseDialog
from modules.utils.constants.CSSClass import CSSClass
from modules.utils.constants.Titles import Titles


class GameOverDialog(BaseDialog):

    def __init__(self, parent, winner_name, sets):
        """
        Default constructor
        @param  parent: The parent window
        """

        super(GameOverDialog, self).__init__(title=self._app_settings.get_label(Titles.END_GAME_DIALOG_TITLE), transient_for=parent)
        
        self._load_container_css(CSSClass.DIALOGS)

        self.connect("key-press-event", self._on_button_pressed)
        
        # Main container
        self._main_grid = Gtk.Grid()
        self._main_grid.set_column_homogeneous(True)
        self._main_grid.set_row_homogeneous(False)

        # Add the labels
        row = 0;
        # Game over
        self._main_grid.attach(self._create_label(CSSClass.GAME_OVER_DIALOG_DESCRIPTION, self._app_settings.get_label(Titles.GAME_OVER_DIALOG_WINNER_MSG)), 0, row, 6, 1)
        
        row = 1;
        # Winner
        self._main_grid.attach(self._create_label(CSSClass.GAME_OVER_DIALOG_WINNER_MSG, self._app_settings.get_label(Titles.END_GAME_DIALOG_WINNER)), 0, row, 6, 1)

        row = 2;
        # Player name
        self._main_grid.attach(self._create_label(CSSClass.GAME_OVER_DIALOG_WINNER_NAME, winner_name), 0, row, 6, 1)

        for game_set in sets:
            row += 1
            self._main_grid.attach(self._create_label(CSSClass.GAME_OVER_DIALOG_WINNER_SETS, game_set), 0, row, 6, 1)
            
        row += 1;

        # Select button
        self._main_grid.attach(self._create_label(CSSClass.BTN_SELECT, self._app_settings.get_label(Titles.BTN_SELECT)), 1, row, 4, 1)

        self.get_content_area().pack_start(self._main_grid, True, True, 0)
        self.show_all()
  
    def _on_button_pressed(self, event, user_data):  # @UnusedVariable
        """
        Handle the keyboard key press
        @param event: The keyboard event
        @param user_data: The pressed user data
        """
        if user_data.keyval in self.get_button_codes():
            self.handle_button_action(user_data.keyval)
            
        return True

    def handle_button_action(self, code): # @UnusedVariable
        """
        Handle the event caused by pressing a valid button
        """
        self.hide()

    def do_show_and_get_info(self):
        """
        Show the dialog and return the selected option
        """
        self.start_monitor_gpio_buttons(self.handle_button_action)        
        self.run()
        self.do_close_screen()
        return None
