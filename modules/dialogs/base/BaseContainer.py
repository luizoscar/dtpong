# -*- coding: utf-8 -*-
'''
###############################################################################################'
 BaseContainer.py

MIT License

Copyright (c) 2019 Luiz Oscar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


###############################################################################################'
 @class Module used as the base for all windows 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
import logging

from gi.repository import Gtk, GLib

from modules.utils.Utils import Utils
from modules.utils.AppSettings import AppSettings
import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library
from modules.utils.constants.CSSClass import CSSClass


class BaseContainer(object):
    _utils = Utils()
    _app_settings = AppSettings()

    def configure_gpio(self):
        """
        Configure the GPIO buttons
        Note: This method should be called once, when the application starts 
        """
        GPIO.setwarnings(True)
        GPIO.setmode(GPIO.BCM) # Use GPIO pin numbering

        self.debug("Configuring the GPIO buttons...")
        for chan in self.get_gpio_channels():
            self.debug("Configuring GPIO pin "+str(chan))
            GPIO.setup(chan , GPIO.IN, pull_up_down=GPIO.PUD_DOWN) 

    def release_gpio(self):
        """
        Release the GPIO
        Note: This method should be called once, when the app is closing 
        """
        self.debug("Cleaning up the GPIO settings")
        GPIO.cleanup() # Clean up
            
    def stop_monitor_gpio_buttons(self):
        """
        Stop monitoring the GPIO buttons
        Note: This method is supposed to be called every time before showing a new Dialog
        """
        self.debug("Stop listening GPIO buttons")
        for channel in self.get_gpio_channels():
            try:
                GPIO.remove_event_detect(channel)
            except:
                self.debug("GPIO pin "+str(channel)+" was not monitored!")
            

    def start_monitor_gpio_buttons(self, callback_method_name):
        """
        Start monitoring the GPIO buttons
        Note: This method is supposed to be called inside each new Dialog
        """
        self.debug("Start listening GPIO buttons.")
        for channel in self.get_gpio_channels():
            try:
                GPIO.remove_event_detect(channel)
            finally:
                GPIO.add_event_detect(channel, GPIO.RISING, bouncetime=250)              
                GPIO.add_event_callback(channel, callback_method_name)
            
    def is_select(self, code):
        """
        Check if the pressed button is the Select button
        """
        pressed = code == self._app_settings._KEY_SELECT or code == self._app_settings._GPIO_SELECT
        if pressed:
            self.debug("Pressed Select with code {}".format(code))
        return pressed
    
    def is_p1_up(self, code):
        """
        Check if the pressed button is the P1 UP button
        """
        pressed = code == self._app_settings._KEY_P1_UP or code == self._app_settings._GPIO_P1_UP
        if pressed:
            self.debug("Pressed Player 1 + with code {}".format(code))
        return pressed

    def is_p2_up(self, code):
        """
        Check if the pressed button is the P2 UP button
        """
        pressed = code == self._app_settings._KEY_P2_UP or code == self._app_settings._GPIO_P2_UP
        if pressed:
            self.debug("Pressed Player 2 + with code {}".format(code))
        return pressed

    def is_p1_down(self, code):
        """
        Check if the pressed button is the P1 DOWN button
        """
        pressed = code == self._app_settings._KEY_P1_DOWN or code == self._app_settings._GPIO_P1_DOWN
        if pressed:
            self.debug("Pressed Player 1 - with code {}".format(code))
        return pressed

    def is_p2_down(self, code):
        """
        Check if the pressed button is the P2 DOWN button
        """
        pressed = code == self._app_settings._KEY_P2_DOWN or code == self._app_settings._GPIO_P2_DOWN
        if pressed:
            self.debug("Pressed Player 2 - with code {}".format(code))
        return pressed
    
    def get_button_codes(self):
        """
        Get the key codes supported by the interface
        """
        return [self._app_settings._KEY_P1_UP, self._app_settings._KEY_P1_DOWN, self._app_settings._KEY_P2_UP, self._app_settings._KEY_P2_DOWN, self._app_settings._KEY_SELECT]

    def get_gpio_channels(self):
        """
        Get the GPIO channels supported by the interface
        """
        return [self._app_settings._GPIO_P1_UP, self._app_settings._GPIO_P1_DOWN, self._app_settings._GPIO_P2_UP, self._app_settings._GPIO_P2_DOWN, self._app_settings._GPIO_SELECT]

            
    def do_close_screen(self):
        GLib.idle_add(self.stop_monitor_gpio_buttons)
        self.destroy()
        
    def debug(self, msg=''):
        '''
        Log a debug message
        '''
        logging.info(self.__class__.__name__+" - "+msg)
    
    def get_app_settings(self, xml_tag):
        '''
        Retrieve a system settings
        '''
        if self._app_settings is None:
            self._app_settings = AppSettings()
        return self._app_settings.get_app_settings(xml_tag)
    
    def _show_message(self, title, msg):
        """
        Show a message to the user.
        Note: This method always return None so it can stop the processing

        @param title: The dialog title
        @param msg: The dialog message
        """
    
        self.debug("Message to the user: " + title + ": " + msg)
        # Exibe um Dialog de aviso
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.CLOSE, title)
        dialog.format_secondary_text(msg)
        dialog.run()
        dialog.destroy()
        return None
    
    def _ask_question(self, title, msg):
        '''
        Ask a question to the user

        @param title: The dialog title
        @param msg: The dialog message
        '''
        
        self.debug("Question to the user: " + title + ": " + msg)
        # Solicita a confirmação
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.QUESTION, Gtk.ButtonsType.YES_NO, title)
        dialog.format_secondary_text(msg)
        response = dialog.run()
        dialog.destroy()
        return response == Gtk.ResponseType.YES
            
    def _create_text_combo(self, options):
        """
        Create a text combo box filled with a list of options.
        
        @param options: The list of text items
        """
    
        # Create the combo
        combo_box = Gtk.ComboBoxText()
        # Add the items
        
        for option in options:
            combo_box.append_text(option)
    
        return combo_box
    
    def _do_hide_and_clear_combo(self, combo, label):
        """
        Clear a combo an hide his relative label.
    
        @param combo: The combo to be cleared
        @param label: The label to be hide  
        """
        combo.remove_all()
        combo.hide()
        label.hide()
    
    def _create_icon_and_label_item(self, label, icon, action):
        """
        Create a MenuItem with an icon

        @param label: The button title
        @param icon: The GTK icon name
        @param action: The button click action method
        """
        self.debug(" - Creating MenuItem: " + label)
        button = Gtk.MenuItem.new()
        b_grid = Gtk.Grid()
        b_grid.set_column_spacing(10)
        b_grid.attach(Gtk.Image.new_from_icon_name(icon, Gtk.IconSize.BUTTON), 0, 0, 1, 1)
        b_grid.attach(Gtk.Label(label=label, halign=Gtk.Align.CENTER), 1, 0, 1, 1)
        b_grid.show_all()
        button.add(b_grid)
        button.connect("activate", action)
        return button
    
    def _create_icon_and_label_button(self, label, icon, action):
        """
        Create a button with an icon

        @param label: The button title
        @param icon: The GTK icon name
        @param action: The button click action method
        """
    
        self.debug(" - Creating button: " + label)
        button = Gtk.Button.new()
        b_grid = Gtk.Grid()
        b_grid.set_column_spacing(10)
        b_grid.set_row_homogeneous(True)
        b_grid.attach(Gtk.Image.new_from_icon_name(icon, Gtk.IconSize.BUTTON), 0, 0, 1, 1)
        b_grid.attach(Gtk.Label(label=label, halign=Gtk.Align.CENTER), 1, 0, 1, 1)
        b_grid.show_all()
        button.add(b_grid)
        button.connect("clicked", action)
        return button
    
    def _compare_tree_rows(self, model, row1, row2, user_data):  # @UnusedVariable
        """
        Sort method used to compare TreeView rows.
        
        @param model: The tree model
        @param row1: The first row  
        @param row2: The second row  
        @param user_data: Tree data  

        """
        sort_column, a = model.get_sort_column_id()  # @UnusedVariable
        value1 = model.get_value(row1, sort_column)
        value2 = model.get_value(row2, sort_column)
    
        if value1 < value2:
            return -1
        elif value1 == value2:
            return 0
        else:
            return 1

    def _create_label(self, style=None, title=None):
        """
        Format a label, adding the CSS class style name, and value, if specified
        @param label: The label to be formatted
        @param style: The CSS style name
        @param title: The label title 
        """
        label = Gtk.Label(halign=Gtk.Align.FILL)
        self._set_label_css(label,style, title)
            
        return label
    
    def _set_label_css(self, label, style=None, title=None):
        """
        Set the CSS style class and the text of an existing label
        @param label: The label to be modified
        @param style: The CSS style name
        @param title: The label title
        """
        self._remove_label_css(label)
        
        if style is not None:
            style_context = label.get_style_context()
            style_context.add_class(style)
            # Buttons must also have the base class
            if style in [CSSClass.BTN_P1_MINUS, CSSClass.BTN_P1_PLUS, CSSClass.BTN_P2_MINUS, CSSClass.BTN_P2_PLUS, CSSClass.BTN_SELECT]:
                style_context.add_class(CSSClass.BTN_BASE)
        
        if title is not None:
            label.set_text(title)

    def _has_css_class(self, label, style):
        """
        Check if a label has a CSS class
        @param label: The label to be verified
        @param style: The CSS style name
        """
        return label.get_style_context().has_class(style)
        
    def _remove_label_css(self, label):
        """
        Remove all CSS class from a label
        @param label: The label that will be modified
        """
        style_context = label.get_style_context()
        for style in style_context.list_classes():
            style_context.remove_class(style)

    def _load_container_css(self, name):
        """
        Load the CSS from a GTK Container
        @param name: The name of the CSS class used by the container
        """
        self._app_settings.load_css_context()
        self.get_style_context().add_class(name)
        self.set_border_width(10)
        
            
class BaseWindow(Gtk.Window, BaseContainer):
    '''
    Base class for GTK Window
    '''

    def __init__(self, **kwds):
        super(BaseWindow, self).__init__(**kwds)

    
class BaseDialog(Gtk.Dialog, BaseContainer):
    '''
    Base class for GTK Dialogs
    '''

    def __init__(self, **kwds):
        super(BaseDialog, self).__init__(**kwds)

