# -*- coding: utf-8 -*-
'''
###############################################################################################'
 PlayerChooseDialog.py

MIT License

Copyright (c) 2019 Luiz Oscar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


###############################################################################################'
 @class Dialog used to select two players 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from gi.repository import Gtk, GLib

from modules.dialogs.base.BaseContainer import BaseDialog
from modules.utils.constants.CSSClass import CSSClass
from modules.utils.constants.Titles import Titles


class PlayerChooseDialog(BaseDialog):

    _player_list = []
    _last_user_index = 0
    _selected_index = 0
    _labels = [] 
    
    def __init__(self, parent, player_list, dialog_title, dialog_description):
        super(PlayerChooseDialog, self).__init__(title=dialog_title, transient_for=parent)
        
        self._load_container_css(CSSClass.DIALOGS)

        self.connect("key-press-event", self._on_button_pressed)

        self._player_list = player_list
        self._last_user_index = 0
        self._selected_index = 0
        self._labels = [] 
        
        # Main container
        self._main_grid = Gtk.Grid()
        self._main_grid.set_column_homogeneous(True)
        self._main_grid.set_row_homogeneous(True)

        row = 0;
        # Add the dialog Description

        self._main_grid.attach(self._create_label(CSSClass.PLAYER_REMOVE_DIALOG_DESCRIPTION, dialog_description), 0, row, 6, 1)
        
        list_size = len(player_list)
        if list_size > 3:
            row += 1;
            # P1 Button UP
            self._main_grid.attach(self._create_label(CSSClass.BTN_P1_PLUS, self._app_settings.get_label(Titles.BTN_P1_PLUS)), 0, row, 1, 1)
            
            # Next page
            self._main_grid.attach(self._create_label(CSSClass.UNSELECTED_ITEM, self._app_settings.get_label(Titles.PLAYER_REMOVE_DIALOG_NEXT_PAGE)), 1, row, 5, 1)
        
        if list_size > 0:
            row += 1;
            # P1 Button DOWN
            self._main_grid.attach(self._create_label(CSSClass.BTN_P1_MINUS, self._app_settings.get_label(Titles.BTN_P1_MINUS)), 0, row, 1, 1)

            # Option 2
            self._label_op2 = self._create_label(CSSClass.UNSELECTED_ITEM, player_list[0])
            self._labels.append(self._label_op2)
            self._main_grid.attach(self._label_op2, 1, row, 5, 1)

        if list_size > 1:
            row += 1;
            # P2 Button UP
            self._main_grid.attach(self._create_label(CSSClass.BTN_P2_PLUS, self._app_settings.get_label(Titles.BTN_P2_PLUS)), 0, row, 1, 1)

            # Option 3
            self._label_op3 = self._create_label(CSSClass.UNSELECTED_ITEM, player_list[1])
            self._labels.append(self._label_op3)
            self._main_grid.attach(self._label_op3, 1, row, 5, 1)

        if list_size > 2:
            row += 1;
            # P2 Button UP
            self._main_grid.attach(self._create_label(CSSClass.BTN_P2_MINUS, self._app_settings.get_label(Titles.BTN_P2_MINUS)), 0, row, 1, 1)

            # Option 4
            self._label_op4 = self._create_label(CSSClass.UNSELECTED_ITEM, player_list[2])
            self._labels.append(self._label_op4)
            self._main_grid.attach(self._label_op4, 1, row, 5, 1)

        self._selected_index = 0 
        self._last_user_index = min(list_size - 1, 2)
        # Select button
        row += 1;
        self._label_select = self._create_label(CSSClass.BTN_SELECT, self._app_settings.get_label(Titles.BTN_SELECT))
        self._main_grid.attach(self._label_select, 1, row, 5, 1)

        self.get_content_area().pack_start(self._main_grid, True, True, 0)
        self._draw_screen()
        self.show_all()
    
    def _draw_screen(self):
        """
        Draw the labels on the screen
        """
        for label in self._labels:
            self._set_label_css(label, CSSClass.UNSELECTED_ITEM, label.get_text())
        
        selected_label = self._labels[self._selected_index]
        self._set_label_css(selected_label, CSSClass.SELECTED_ITEM, selected_label.get_text())

    def _on_button_pressed(self, event, user_data):  # @UnusedVariable
        """
        Handle the keyboard key press
        @param event: The keyboard event
        @param user_data: The pressed user data
        """
        if user_data.keyval in self.get_button_codes():
            self.handle_button_action(user_data.keyval)
            
        return True

    def handle_button_action(self, code):
        """
        Handle the event caused by pressing a valid button
        """
        list_size = len(self._player_list)
        if self.is_p1_up(code) and list_size > 3:
            self._draw_next_page()
        elif self.is_p1_down(code)  and list_size > 0:
            self._selected_index = 0
        elif self.is_p2_up(code)and list_size > 1:
            self._selected_index = 1       
        elif self.is_p2_down(code) and list_size > 2:
            self._selected_index = 2       
        elif self.is_select(code):
            self.hide()
        
        GLib.idle_add(self._draw_screen)
        
    def _draw_next_page(self):
        """
        Draw the next page
        """

        list_size = len(self._player_list) - 1
        
        for index in range(0, 3):
            self._last_user_index += 1
            if self._last_user_index > list_size:
                self._last_user_index = 0

            self._labels[index].set_text(self._player_list[self._last_user_index])

        self._selected_index = 0

    def do_show_and_get_info(self):
        """
        Show the dialog and return the selected option
        """
        self.start_monitor_gpio_buttons(self.handle_button_action)        
        self.run()
        self.do_close_screen()
        return self._player_list[self._selected_index]
    
