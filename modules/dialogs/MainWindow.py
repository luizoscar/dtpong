# -*- coding: utf-8 -*-
'''
###############################################################################################'
 BaseContainer.py

MIT License

Copyright (c) 2019 Luiz Oscar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


###############################################################################################'
 @class Module used as the base for all windows 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from gi.repository import Gtk, GLib

import sys
import time
import operator
from modules.dialogs.base.BaseContainer import BaseWindow
from modules.dialogs.base.MutipleSelectionDialog import MutipleSelectionDialog
from modules.dialogs.base.PlayerChooseDialog import PlayerChooseDialog
from modules.dialogs.players.PlayerReaderDialog import PlayerReaderDialog
from modules.dialogs.players.PlayerSelectionDialog import PlayerSelectionDialog
from modules.dialogs.GameOverDialog import GameOverDialog
from modules.utils.constants.CSSClass import CSSClass
from modules.utils.constants.Titles import Titles
from modules.utils.AppSettings import AppSettings
from datetime import datetime


class MainWindow(BaseWindow):
    
    # Player 1 settings
    _p1_score = 0
    _p1_sets = 0
    _p1_name = "Player 1"
    
    # Player 2 settings
    _p2_score = 0
    _p2_sets = 0
    _p2_name = "Player 1"

    # Current game start time
    _start_time = 0
    
    # Played Sets
    _game_sets = []

    _current_gamer = 0

    # List of player names
    _player_list = []
    # List of player labels
    _player_labels = []

    _score_updated = False

    def __init__(self):

        Gtk.Window.__init__(self, title=self._app_settings.get_label(Titles.APPLICATION_TITLE))

        self.set_icon_name("application-x-executable")
        Gtk.Settings().set_property('gtk_button_images', True)

        self._load_container_css(CSSClass.MAIN_WINDOW)
                
        if (self._app_settings.is_debug_mode()):
            self.maximize()
        else:
            self.fullscreen()
            
        self.connect('delete-event', self._on_close)
        self.connect("key-press-event", self._on_button_pressed)

        self._player_list = []
        self._player_labels = []
        self._game_sets = []

        # Main container
        self._main_grid = Gtk.Grid()
        self._main_grid.set_column_homogeneous(True)
        self._main_grid.set_row_homogeneous(True)

        # Add the labels
        row = 0;
        # Player 1 indicator
        self._label_p1_ball = self._create_label(CSSClass.PLAYER_SERVE_INDICATOR, self._app_settings.get_label(Titles.PLAYER_1_SERVE))
        self._main_grid.attach(self._label_p1_ball, 0, row, 1, 1)

        # Player 1 Name
        self._p1_name = self._app_settings.get_label(Titles.DEFAULT_PLAYER_1_NAME)
        self._label_p1_name = self._create_label(CSSClass.PLAYER_1_NAME)
        self._main_grid.attach(self._label_p1_name, 1, row, 3, 1)

        # Player 2 name
        self._p2_name = self._app_settings.get_label(Titles.DEFAULT_PLAYER_2_NAME)
        self._label_p2_name = self._create_label(CSSClass.PLAYER_2_NAME)
        self._main_grid.attach(self._label_p2_name, 6, row, 3, 1)

        # Player 2 indicator
        self._label_p2_ball = self._create_label()
        self._main_grid.attach(self._label_p2_ball, 9, row, 1, 1)

        row = 1;
        # Player 1 sets counter
        self._label_p1_sets_score = self._create_label(CSSClass.PLAYER_1_SET_COUNTER)
        self._main_grid.attach(self._label_p1_sets_score, 0, row, 1, 1)

        # Player 1 score
        self._label_p1_score = self._create_label(CSSClass.PLAYER_1_SCORE)
        self._main_grid.attach(self._label_p1_score, 1, row, 3, 4)

        # Current time
        self._label_time = self._create_label(CSSClass.CURRENT_TIME)
        self._main_grid.attach(self._label_time, 4, row, 2, 1)

        # Player 2 score
        self._label_p2_score = self._create_label(CSSClass.PLAYER_2_SCORE)
        self._main_grid.attach(self._label_p2_score, 6, row, 3, 4)

        # Player 2 sets counter
        self._label_p2_sets_score = self._create_label(CSSClass.PLAYER_2_SET_COUNTER)
        self._main_grid.attach(self._label_p2_sets_score, 9, row, 1, 1)
        
        row = 2

        # Game mode
        self._label_game_mode = self._create_label(CSSClass.GAME_MODE)
        self._main_grid.attach(self._label_game_mode, 4, row , 2, 1)

        row = 4;

        # Game duration
        self._label_duration_time = self._create_label(CSSClass.GAME_DURATION)
        self._main_grid.attach(self._label_duration_time, 4, row, 2, 1)

        row = 5;

        # Next players
        self._main_grid.attach(self._create_label(CSSClass.NEXT_PLAYERS, self._app_settings.get_label(Titles.NEXT_PLAYERS)), 1, row, 8, 1)

        # Create the labels for the next players
        col = -1
        for i in range(16):
            
            index = i + 1
            row += 1
            if operator.mod(index, 4) == 1:
                col += 2
                row = 6
            
            title = self._player_list[i] if len(self._player_list) > i else""
            label = self._create_label(CSSClass.NEXT_PLAYER_NAME, str(index) + ": " + title)
            self._main_grid.attach(label, col, row, 2, 1)
            self._player_labels.append(label)
                    
        self._start_game()

        # Start the timer thread
        GLib.timeout_add_seconds(1,self._update_timers)
    
        self.add(self._main_grid)
        self.configure_gpio()
        self.start_monitor_gpio_buttons(self.handle_button_action)

    def _update_timers(self):
        """
        Update the game time and the current time
        """
        self._label_duration_time.set_text(self._utils.get_formated_minutes(time.time() - self._start_time))
        self._label_time.set_text(datetime.now().strftime('%H:%M:%S'))
        return True

    def _on_button_pressed(self, event, user_data):  # @UnusedVariable
        """
        Handle the keyboard key press
        @param event: The keyboard event
        @param user_data: The pressed user data
        """
        if user_data.keyval in self.get_button_codes():
            self.handle_button_action(user_data.keyval)
            
        return True

    def handle_button_action(self, code):
        """
        Handle the event caused by pressing a valid button
        """
        if self.is_p1_up(code):
            self._update_p1_score(1)
        elif self.is_p1_down(code):
            self._update_p1_score(-1)
        elif self.is_p2_up(code):
            self._update_p2_score(1)
        elif self.is_p2_down(code):
            self._update_p2_score(-1)
        elif self.is_select(code):
            self.stop_monitor_gpio_buttons()
            GLib.idle_add(self._show_application_menu)

    def _update_p1_score(self, amount):
        """
        Increment / Decrement the player 1 score
        """
        if amount < 0 and self._p1_score == 0:
            return

        self._p1_score = max(0, self._p1_score + amount)
        self._current_gamer = 0
        self._score_updated = True
        
        GLib.idle_add(self._update_game_progess)

    def _update_p2_score(self, amount):
        """
        Increment / Decrement the player 2 score
        """
        if amount < 0 and self._p2_score == 0:
            return

        self._p2_score = max(0, self._p2_score + amount)
        self._current_gamer = 1
        self._score_updated = True
        
        GLib.idle_add(self._update_game_progess)
       
    def _show_application_menu(self):
        """
        Show the application menu
        """
        self.debug("Showing the application menu")
        
        # Show the options dialog
        options = [self._app_settings.get_label(Titles.START_NEW_GAME),
                   self._app_settings.get_label(Titles.APPEND_NEW_PLAYER),
                   self._app_settings.get_label(Titles.REMOVE_EXISTING_USER),
                   self._app_settings.get_label(Titles.REGISTER_RFID_USER)]
        
        dialog = MutipleSelectionDialog(self, self._app_settings.get_label(Titles.MULTIPLE_SELECTION_DIALOG_TITLE), options)
        selected_option = dialog.do_show_and_get_info()
        dialog.destroy()
        
        self.debug("Selected option is " + str(options[selected_option]))

        if selected_option == 0:
            GLib.idle_add(self._show_start_new_game)
        elif selected_option == 1:
            GLib.idle_add(self._show_append_player)
        elif selected_option == 2:
            GLib.idle_add(self._show_remove_player)
        elif selected_option == 3:
            GLib.idle_add(self._show_register_player_rfid)

    def _show_start_new_game(self):
        """
        Show the create new game dialog
        """
        self.debug("Creating a new game")
        
        # Select the game play rules
        options = [self._app_settings.get_label(Titles.NEW_GAME_MODE_TOURNAMENT),
                   self._app_settings.get_label(Titles.NEW_GAME_MODE_PLAY_NO_STOP),
                   self._app_settings.get_label(Titles.NEW_GAME_MODE_PLAY_3_WIN_1_OUT),
                   self._app_settings.get_label(Titles.NEW_GAME_MODE_PLAY_3_WIN_2_OUT),
                   ]
        dialog = MutipleSelectionDialog(self, self._app_settings.get_label(Titles.MULTIPLE_SELECTION_DIALOG_TITLE), options)
        selected_option = dialog.do_show_and_get_info()
        self.debug("The New game mode is " + str(options[selected_option]))

        # Configure the game mode type
        self._app_settings.set_game_mode(selected_option)

        # Select the users from the tournament
        if selected_option == AppSettings.GAME_MODE_TOURNAMENT:
            GLib.idle_add(self._select_tournament_players)
        else:
            self.start_monitor_gpio_buttons(self.handle_button_action)
            self._start_game()

    def _select_tournament_players(self):
        """
        Show the tournament player selection dialog
        """
        self.debug("Creating a new tournament game")

        dialog = PlayerSelectionDialog(self)
        players = dialog.do_show_and_get_info()
        dialog.destroy()

        # Add the current players to the end of the list
        self._player_list.append(self._p1_name)
        self._player_list.append(self._p2_name)
        self._p1_name = players[0]
        self._p2_name = players[1]

        self._p1_sets = 0
        self._p2_sets = 0
        self._score_updated = True
        self.start_monitor_gpio_buttons(self.handle_button_action)
        self._start_game()

    def _show_append_player(self):
        """
        Append a new user to the player list
        """
        self.debug("Adding a new player")
        
        # Read the player and add to the list
        dialog = PlayerReaderDialog(self)
        info = dialog.do_show_and_get_info()
        dialog.destroy()
        user_name = info[0]

        if not user_name in self._player_list:
            self._player_list.append(user_name)
            self._draw_waiting_list()

        self.start_monitor_gpio_buttons(self.handle_button_action)
               
    def _show_remove_player(self):
        """
        Remove a player from the list
        """
        if len(self._player_list) > 0:
            self.debug("Remove an existing player")
            title = self._app_settings.get_label(Titles.PLAYER_REMOVE_DIALOG_TITLE)
            desc = self._app_settings.get_label(Titles.PLAYER_REMOVE_DIALOG_DESCRIPTION)
            
            dialog = PlayerChooseDialog(self, self._player_list, title, desc)
            player_to_remove = dialog.do_show_and_get_info()
            dialog.destroy()
            self.debug("Removing player "+player_to_remove+" from the list")

            self._player_list.remove(player_to_remove)
        self._draw_waiting_list()
        self.start_monitor_gpio_buttons(self.handle_button_action)
    
    def _show_register_player_rfid(self):
        """
        Register the user RFID code
        """
        player_map = self._app_settings.get_player_list()
        players = []
        for name in  player_map:
            players.append(name + " / " + player_map[name])
            
        if len(players) > 0:
            self.debug("Configure an existing player")
            title = self._app_settings.get_label(Titles.PLAYER_REMOVE_DIALOG_TITLE)
            desc = self._app_settings.get_label(Titles.PLAYER_REMOVE_DIALOG_DESCRIPTION)
            
            dialog = PlayerChooseDialog(self, players, title, desc)
            self._player_to_edit = dialog.do_show_and_get_info()
            dialog.destroy()
            GLib.idle_add(self._edit_player)
    
    def _edit_player(self):
        """
        Edit the RFID of a player
        """
        self.debug("Associating the RFID of the player")
        
        # Read the player and add to the list
        dialog = PlayerReaderDialog(self, self._player_to_edit)
        info = dialog.do_show_and_get_info()
        dialog.destroy()
        user_name = info[0]
        user_rfid = info[1]
        self.debug("Registering player " + user_name + " with RFID " + user_rfid)
        self._app_settings.register_player(info)
        self.start_monitor_gpio_buttons(self.handle_button_action)

    def _start_game(self):
        """
        Start a new game
        """
        self.debug("Starting a new game [" + self._p1_name + " vs " + self._p2_name + "]  mode = " + str(self._app_settings.get_game_mode()))
        
        # Get the initial time
        self._start_time = time.time()

        # Clear the previous scores
        self._p1_score = 0
        self._p2_score = 0
        self._score_updated = True

        self._game_sets = []
    
        # Set the player names
        self._label_p1_name.set_text(self._p1_name)
        self._label_p2_name.set_text(self._p2_name)

        # Get the play mode title
        game_mode = self._app_settings.get_game_mode()
        
        if game_mode == AppSettings.GAME_MODE_TOURNAMENT:
            game_title = self._app_settings.get_label(Titles.GAME_MODE_TOURNAMENT)
        elif game_mode == AppSettings.GAME_MODE_PLAY_NO_STOP:
            game_title = self._app_settings.get_label(Titles.GAME_MODE_PLAY_NO_STOP)
        elif game_mode == AppSettings.GAME_MODE_PLAY_3_WIN_1_OUT:
            game_title = self._app_settings.get_label(Titles.GAME_MODE_PLAY_3_WIN_1_OUT)
        else:
            game_title = self._app_settings.get_label(Titles.GAME_MODE_PLAY_3_WIN_2_OUT)
            
        self._label_game_mode.set_text(game_title)
        GLib.idle_add(self._update_game_progess)
    
    def _append_game_set(self):
        """
        Append a game result to the result list
        """
        self._game_sets.append(self._p1_name + " (" + str(self._p1_score) + ") X (" + str(self._p2_score) + ") " + self._p2_name)

    def _show_end_game_play(self):
        """
        Finishing the game
        """
        
        # Add the looser to the end of the list and get the new player
        if self._p1_score > self._p2_score:
            self._player_list.append(self._p2_name)
            self._p2_name = self._get_next_player()
            self._p2_sets = 0
        else:
            self._player_list.append(self._p1_name)
            self._p1_name = self._get_next_player()
            self._p1_sets = 0
        
        # Each 3 win, winner goes out
        game_mode = self._app_settings.get_game_mode()
        if game_mode != AppSettings.GAME_MODE_PLAY_NO_STOP:
            if self._p1_score > self._p2_score:
                self._p1_sets += 1
            else:
                self._p2_sets += 1
            
            # The value of Config.GAME_MODE_PLAY_RULES is the number of skipped plays
            jump_to = min(len(self._player_list) , game_mode) - 1
            if self._p1_sets >= 3:
                self._player_list.insert(jump_to, self._p1_name)
                self._p1_name = self._get_next_player()
                self._p1_sets = 0
                self._p2_sets = 0 
            elif self._p2_sets >= 3:
                self._player_list.insert(jump_to, self._p2_name)
                self._p2_name = self._get_next_player()
                self._p2_sets = 0
                self._p1_sets = 0
            
        self._start_game()
        
    def _get_next_player(self):
        """
        Get the next player name
        """
        if len(self._player_list) == 0:
            self._player_list.append(self._app_settings.get_label(Titles.DEFAULT_PLAYER_1_NAME))
        
        name = self._player_list[0]
        self._player_list.remove(name)
        self.debug("Next player is: " + name)
        return name
        
    def _show_end_game_tournament(self):
        """
        End the game when in tournament mode
        """
        self.stop_monitor_gpio_buttons()

        # Get the winner name and show the dialog
        winner_name = self._p1_name if self._p1_score > self._p2_score else self._p2_name
        
        dialog = GameOverDialog(self, winner_name, self._game_sets)
        dialog.do_show_and_get_info()
        dialog.destroy()
        self._app_settings.set_game_mode(AppSettings.GAME_MODE_PLAY_3_WIN_2_OUT)

        self._p1_name = self._get_next_player() 
        self._p2_name = self._get_next_player()
        self._p1_sets = 0
        self._p2_sets = 0

        self._start_game()  
        self.start_monitor_gpio_buttons(self.handle_button_action)

    def _swap_players(self):
        """
        Swap the players information
        """
        self._append_game_set()
        
        self._p1_score = 0
        self._p2_score = 0
        
        sets = self._p1_sets
        self._p1_sets = self._p2_sets
        self._p2_sets = sets

        name = self._p1_name
        self._p1_name = self._p2_name
        self._p2_name = name

        self._label_p1_name.set_text(self._p1_name)
        self._label_p2_name.set_text(self._p2_name)
    
    def _update_game_progess(self):
        """
        Update the game progress
        """
        min_points = min(self._p1_score, self._p2_score)
        max_points = max(self._p1_score, self._p2_score)
        
        # Check if any player won
        set_won = max_points > (self._app_settings.get_set_points() - 1) and (max_points - min_points) > 1
        if set_won:
            # Check if game is over
            if self._app_settings.get_game_mode() != AppSettings.GAME_MODE_TOURNAMENT:
                self._show_end_game_play()
            elif self._p1_score > self._p2_score:
                self._p1_sets += 1
                self._swap_players()
            elif self._p2_score > self._p1_score:                
                self._p2_sets += 1
                self._swap_players()
            
            # check if tournament game is over  
            if self._app_settings.get_game_mode() == AppSettings.GAME_MODE_TOURNAMENT and max(self._p1_sets, self._p2_sets) > 1:
                GLib.idle_add(self._show_end_game_tournament)
        
        # Update the score
        self._label_p1_sets_score.set_text(str(self._p1_sets))
        self._label_p2_sets_score.set_text(str(self._p2_sets))
        total_score = self._p1_score + self._p2_score
               
        # Update the Sets 
        self._label_p1_score.set_text(str(self._p1_score))
        self._label_p2_score.set_text(str(self._p2_score))

        # Update the serve indicator
        self._update_serve_indicators(total_score)
        
        # Draw the player list
        self._draw_waiting_list()
    
    def _update_serve_indicators(self, total_score):
        """
        Update the serve indicator
        """
        if not self._score_updated:
            return
        
        if self._app_settings.is_winner_keeps_serving():
            if self._current_gamer == 0:
                self._set_label_css(self._label_p1_ball, CSSClass.PLAYER_SERVE_INDICATOR, self._app_settings.get_label(Titles.PLAYER_1_SERVE))
                self._label_p2_ball.set_text("")
                self._remove_label_css(self._label_p2_ball)
            else:         
                self._label_p1_ball.set_text("")
                self._remove_label_css(self._label_p1_ball)         
                self._set_label_css(self._label_p2_ball, CSSClass.PLAYER_SERVE_INDICATOR, self._app_settings.get_label(Titles.PLAYER_1_SERVE))
        else:
            # NOTE: The control of player serve is based on the CSS class
            if operator.mod(total_score, 2) == 0:
                if self._has_css_class(self._label_p2_ball, CSSClass.PLAYER_SERVE_INDICATOR):
                    self._set_label_css(self._label_p1_ball, CSSClass.PLAYER_SERVE_INDICATOR, self._app_settings.get_label(Titles.PLAYER_1_SERVE))
                    self._label_p2_ball.set_text("")
                    self._remove_label_css(self._label_p2_ball)         
                else:
                    self._label_p1_ball.set_text("")
                    self._remove_label_css(self._label_p1_ball)         
                    self._set_label_css(self._label_p2_ball, CSSClass.PLAYER_SERVE_INDICATOR, self._app_settings.get_label(Titles.PLAYER_1_SERVE))
        
        self._score_updated = False
        
    def _draw_waiting_list(self):
        """
        Draw the list of players waiting to play
        """
        for i in range(16):
            title = self._player_list[i] if len(self._player_list) > i else""
            self._player_labels[i].set_text(str(i + 1) + ": " + title)
            
    def _on_close(self, widget, owner):  # @UnusedVariable
        """
        Close the application and release the logger
        """
        self.stop_monitor_gpio_buttons()
        self.release_gpio()
        sys.exit()        
