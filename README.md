# DTPong - A Table Tennis Scoreboard application

DTPong is a highly customized table tennis scoreboard appplication written in Python with GTK 3 to be used inside a Raspberry pi zero.  
The application has a simple interface, where all commands are acessed using only 5 keyboard keys or 5 buttons attached to the raspberry pi GPIO.  
The application manages the player list. The details of the players are read using a RFID RC522 card reader.  
The interface is highly customized using CSS, and the keys, GPIO pins and applications title are configured using a XML file.  

## Supported games mode

+ Challenge mode:  
  Players will play a Best-of-three playoff. After the game, the result is reported to a REST server, saved to a LOG / XML or a database.  

+ Simple play mode:  
  Players will play a single playoff, the looser is automatically added to the end of the play list and a new game is created with the next player on the list.  

+ Play mode where the 3 times winner stays 1 out
  Players will play a single playoff, the looser is automatically added to the end of the play list and a new game is created with the next player on the list.  
  When a player wins 3 times consecutively, he will stay out for 1 round.

+ Play mode where the 3 times winner stays 2 out
  Players will play a single playoff, the looser is automatically added to the end of the play list and a new game is created with the next player on the list.  
  When a player wins 3 times consecutively, he will stay out for 2 rounds.
  
## Features

+ Manages the player list
+ Show the current time, and the game play time
+ Indicate which player is serving, using 2 serves for each player
+ The interface is customized using CSS see the supported [GTK+ CSS Properties](https://developer.gnome.org/gtk3/stable/chap-css-properties.html)
+ All labels cand be modified on a XML file (Default language is Portuguese)
+ The number of points for the Sets is customized
+ The keyboard key code and Python GPIO pin mapping can be defined on the XML

## Screenshots

### The application window

This screen displays the players name, the number of sets each player has won, the current score, current and game time, as well as the next players queue.  
All visual elements on the screen can be changed by editing the `settings.css` and the `settings.xml` files.

![alt text](images/application.png "Application window")

### The main menu

The main menu, acessible by pressing the Start button on the main screen.  
From this screen, we can start a new game, add / remove a player from the players queue, and configure the player RFID code.  
The options are acessible using the 4 navigation buttons indicated on the left (Players +1 / -1 point)  
To confirm the selection, press the Start button

![alt text](images/menu.png "Application main menu")

### The New game modes

When selecting the first option (new game), it is possible to select between the game modes:  

+ New chalenge
+ New simple play mode
+ New 3 times winner stays 1 out
+ New 3 times winner stays 2 out

![alt text](images/new_game.png "New game modes")

### The New Challenge game mode

When selecting a new challenge, a screen will allow the user to select the players, by pressing the player button, a screen will be displayed allowing to read the RFID card.

![alt text](images/chalenge.png "New chalenge")

### Selecting a new player

This screen will generate a random player name (in case the player does not have a RFID card). When the player pass the card over the RFID reader, the number of the card will be read and compared with the list of players present on the `players.xml` file

![alt text](images/rfid_reader.png "RFID reader")

### The player list

During the play, the loosers will be automatically added to the end of the list. Any new player will be added to the end of the players list.  
If a new challenge is created, the current players will be added to the top of the list, so they can continue playing after the challenge.  
If the player mode is set to 3 wins stays 1 or 3 out, the looser will go to the end of the list, and the winner will be added to the next or 3rd position.

![alt text](images/playlist.png "Player queue")

### Managing the player list

The player list can be changed by removing and adding new players.  
When adding a new player, the RFID reader screen is displayed, after the confirmation, the player is added to the end of the play list.  
When selecting to remove a player from the list, a screen is displayed, allowing to select the player that is going to be removed.  
The screen will display up to 3 players per page, acessible using P1 -1, P2 +1 and P2 -1 buttons.  
P1 +1 button allow to switch to the next page.

![alt text](images/playlist_manage.png "Player queue management")

### Game Over

When a challenge game is over, the application will show a screen with the results of the game.

![alt text](images/game_over.png "Game Over")

## Configuration

### Player list

The player list is stored on the `players.xml` file, on the main dir.  
The XML syntax is:

```xml
<?xml version='1.0' encoding='UTF-8'?>
<players>
    <player rfid="123456789" name="My Player Name" email="my.email@provider.com"/>
</players>
```

The `players.xsd` can be used to validate the xml file.

### Settings

The application labels and settings are stored on the `settings.xml` file.
The XML syntax is:

```xml
<?xml version='1.0' encoding='UTF-8'?>
<config>
    <set_points>11</set_points>
    <game_mode_play_type>2</game_mode_play_type>
    <winner_keeps_serving>False</winner_keeps_serving>
    <key_code_p1_up>49</key_code_p1_up>
    <key_code_p2_up>51</key_code_p2_up>
    <key_code_p1_down>50</key_code_p1_down>
    <key_code_p2_down>52</key_code_p2_down>
    <key_code_select>32</key_code_select>
    <gpio_pin_p1_up>16</gpio_pin_p1_up>
    <gpio_pin_p2_up>19</gpio_pin_p2_up>
    <gpio_pin_p1_down>20</gpio_pin_p1_down>
    <gpio_pin_p2_down>26</gpio_pin_p2_down>
    <gpio_pin_select>13</gpio_pin_select>
    <labels>
        ....
    </labels>
    <debug>False</debug>
</config>

```

The following settings can be changed

|  XML Tag name        | XML type       | Description                                                   |
|:---------------------|:---------------|:--------------------------------------------------------------|
|set_points            | Integer        | The number of points for the set                              |
|game_mode_play_type   | Integer        | The default game play mode [1 to 4]                           |
|winner_keeps_serving  | Boolean        | True so the winner keeps serving, false to swap each 2 points |
|key_code_p1_up        | Integer        | The keyboard code for the Player 1 +1 point button            |
|key_code_p2_up        | Integer        | The keyboard code for the Player 2 +1 point button            |
|key_code_p1_down      | Integer        | The keyboard code for the Player 1 -1 point button            |
|key_code_p2_down      | Integer        | The keyboard code for the Player 2 -1 point button            |
|key_code_select       | Integer        | The keyboard code for the start button                        |
|gpio_pin_p1_up        | Integer        | The raspberry pi GPIO code fot the player 1 +1 point button   |
|gpio_pin_p2_up        | Integer        | The raspberry pi GPIO code fot the player 2 +1 point button   |
|gpio_pin_p1_down      | Integer        | The raspberry pi GPIO code fot the player 1 -1 point button   |
|gpio_pin_p2_down      | Integer        | The raspberry pi GPIO code fot the player 2 -1 point button   |
|gpio_pin_select       | Integer        | The raspberry pi GPIO code fot the start button               |

The labels settings contains the texts displayed on the application screen.  

The `settings.xsd` can be used to validate the xml file.

If the file does not exists, the application will create one with the deafault values

### Visual

Although the label displacement can not be changed (Ex. The player name is always on the top, the play list always on the botton..), the titles, colors and formats can be altered using basic CSS instructions.  
The visual settings can be changed by editing the `settings.css` file.  
The CSS parameters supported by the application can be checked on the [GTK+ CSS Properties](https://developer.gnome.org/gtk3/stable/chap-css-properties.html)
If an unsupported CSS synxtax is detected, the application will close.

## Configuring the raspberry py

To be continued...
```sh
sudo apt-get update
sudo apt-get dist-upgrade
sudo apt-get install python-dev
sudo apt-get install python3-dev
sudo pip install mfrc522
sudo pip3 install mfrc522
```